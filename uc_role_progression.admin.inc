<?php 

/**
* @file
* Administration page callbacks for the UC Role Progression module
*/

/**
* Form builder. Configure UC Role Progression
*
* @ingroup forms
* @see system_settings_form().
*/
function uc_role_progression_settings() {
  // Get an array of roles with internal names as keys and friendly names as values.
  $roles = user_roles($membersonly = TRUE, $permission = NULL);
  $options = array(0 => 'none')+$roles;
  $weeks =  array('1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9, '10' => 10);

  // Allow user to select a role that progresses from each existing role
  foreach ($roles as $key => $value) {
    $specific_options = $options; 
    unset($specific_options[$key]);
  
    $form[$value] = array(
      '#type' => 'fieldset',
      '#title' => $value,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );  
  
    $form[$value]['uc_role_progression_' . $key] = array(
      '#type' => 'select',
      '#title' => t($value . ' is followed by'),
      '#options' =>  $specific_options,
      '#default_value' => variable_get('uc_role_progression_' . $key, array('page')),
      '#description' => t('Select role that follows @role in progression.', array('@role' => $value)),
    );
    
    $form[$value]['uc_role_progression_prerequisites_' . $key] = array(
      '#type' => 'select',
      '#title' => t('Prerequisite roles that allow this progression'),
      '#options' =>  $roles,
      '#multiple' => TRUE,
      '#default_value' => variable_get('uc_role_progression_prerequisites_' . $key, array('page')),
      '#description' => t('This progression is allowed if the user is a member of any of the above prerequisite roles.'),
    );
    
    $form[$value]['uc_role_progression_expiration_' . $key] = array(
      '#type' => 'select',
      '#title' => t('Weeks until role expires'),
      '#description' => t('Select the number of this weeks this roles should last before expiring and triggering another role progression.'),
      '#options' => $weeks,
      '#default_value' => array('1' => 1),
    );

  }
  return system_settings_form($form);
}
